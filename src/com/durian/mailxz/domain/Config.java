package com.durian.mailxz.domain;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.WeakHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.durian.mailxz.domain.fields.EmailFields;
import com.durian.mailxz.exception.ConfigurationException;
import com.durian.mailxz.exception.MessageStoreException;
import com.durian.mailxz.index.MessageIndex;
import com.durian.mailxz.search.StandardSearch;
import com.durian.mailxz.service.MessageService;
import com.durian.mailxz.store.MessageStore;
import com.durian.mailxz.store.VolumeIRService;
import com.durian.mailxz.util.ConfigUtil;

public class Config {
	public static final String DEFAULT_REPOSITRY_ROOT = "repositry";

	protected static final String saltKey = "security.salt";

	protected static final String pbeAlgorithmKey = "security.pbealgorithm";
	protected static final String defaultPBEAlgorithm = "PBEWithMD5AndTripleDES";
	protected static final String tempDirKey = "temp.dir";
	protected static final Object readWriteLock = new Object();
	
	protected static final Log logger = LogFactory.getLog(Config.class);

	protected static final Log audit = LogFactory
			.getLog("com.stimulus.archiva.audit");

	public String repositryRoot = DEFAULT_REPOSITRY_ROOT;
	public String indexFolder = "index";
	public String dataFolder = "dataStore";

	public String lastImportPath = ".";

	protected static Config config = null;
	protected static boolean shutdown = false;

	protected byte[] salt;
	protected String tempDir;

	protected ArrayList<Props> props = new ArrayList<Props>();

	protected Archiver archiver;
	protected Indexer indexer;
	protected Volumes volumes;
	protected VolumeIRService volumeIRService;
	protected String pbeAlgorithm;
	protected static FileSystem filesystem;
	protected Search search;
	protected static ConfigAutoLoadService configAutoLoad = new ConfigAutoLoadService();
	protected EmailFields emailFields;

	protected FetchMessageCallback callback;

	static {
		
   	 startup();
   	 System.setProperty("mail.mime.base64.ignoreerrors", "true");
   	 System.setProperty("mail.mime.applefilenames", "true");
    }
	
	// --------------
	public String getIndexDirectory() {
		return repositryRoot + File.pathSeparator + indexFolder;
	}

	public String getDataDirectory() {
		return repositryRoot + File.pathSeparator + dataFolder;
	}

	private static class ConfigHolder {
		private static Config singleton = null;

		public static Config getSingleton() {
			if (null == singleton) {
				singleton = new Config();
			}
			return singleton;
		}
	}

	public static Config getDefault() {
		return ConfigHolder.getSingleton();
	}

	// <end>------------
	public static synchronized Config getConfig() {
		if (Config.config == null) {
			Config.config = new Config();
		}
		return Config.config;
	}

	public void init(FetchMessageCallback callback) {

		if (callback == null) {
			callback = MessageService.getFetchMessageCallback();
		}
		this.callback = callback;

		domains = new Domains();
  	  roles = new Roles();
//  	  agent = new Agent();
	   	  archiveFilter = new ArchiveFilter();
//	   	  authentication = new Authentication();
//	   	  adIdentity = new ADIdentity();
//	   	  basicIdentity = new BasicIdentity();
//	   	  mailboxConnections = new MailboxConnections();
	   	  archiver = new MessageStore();
	   	  volumes = new Volumes();
	   	  search = new StandardSearch();
	   	  services = new Services();
	   	  indexer = new MessageIndex();
	   	  props = new ArrayList<Props>();
//	   	  milterService = new MilterServerService();
//	   	  smtpService = new SMTPServerService();
//	   	  iapService = new IAPService();
	   	  //autoUpdateService = new AutoUpdateService();
	   	  emailFields = new EmailFields();
//	   	  volumeInfoService = new VolumeInfoService();
//	   	  logFiles = new LogFiles();
	   	  volumeIRService = new VolumeIRService();
//	   	  throttleService = new CPUThrottleService();
	   	  registerProps();
	}

/*	public void initFirstVolumn() {
//		repositry
		if (volumes.getVolumes().size() == 0) {
			try {
				volumes.addVolume(filesystem.getApplicationPath()+"\\repositry\\store\\store0",
						filesystem.getApplicationPath()+"\\repositry\\index\\index0", 5000, false);
			} catch (ConfigurationException e) {
				e.printStackTrace();
			}
		}
	}*/
	
	 public static void startup() {
		   filesystem = new FileSystem(); 
//		   stopBlockFactory = new StopBlockFactory();
		   updateObservers = new WeakHashMap<UpdateObserver,UpdateObserver>();
	   }
	 
	public static void shutdown() {
		if (Config.config != null) {
			Config.config.getServices().stopAll();
		}
		/*
		try {
			Config.config.saveConfiguration(MailArchivaPrincipal.SYSTEM_PRINCIPAL);
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		shutdown = true;
		Config.config = null;
		Config.filesystem.shutdown();
		Config.filesystem = null;
		Config.updateObservers = null;
		// Config.stopBlockFactory.shutdown();
		// Config.stopBlockFactory = null;
	}

	public synchronized void loadSettings(MailArchivaPrincipal principal)
			throws ConfigurationException {
		try {
			configAutoLoad.block();
			volumeIRService.block();
			loadConfiguration(principal);
			try {
				archiver.init();
			} catch (MessageStoreException mse) {
				throw new ConfigurationException(
						"failed to initialize message store.", mse, logger);
			}
			// adIdentity.loadHostsFileEntry();
			// basicIdentity.loadXMLFile();
			emailFields.loadXMLFile();
			try {
				volumes.loadAllVolumeInfo();
			} catch (Exception e) {
			}
			notifyUpdateObservers();
		} finally {
			configAutoLoad.unblock();
			volumeIRService.unblock();
		}
	}

	public synchronized Settings loadConfiguration(
			MailArchivaPrincipal principal) throws ConfigurationException {
		Settings prop = loadConfigurationFile(principal);
		setSalt(prop.getProperty(saltKey));
		setPBEAlgorithm(ConfigUtil.getString(prop.getProperty(pbeAlgorithmKey),
				defaultPBEAlgorithm));
		// applicationVersion =
		// ConfigUtil.getString(settings.getProperty(applicationVersionKey),defaultApplicationVersion);
		String tmpDir = prop.getProperty(tempDirKey);

		if (tmpDir == null) {
			tempDir = newTempDir();
		} else {
			tempDir = tmpDir;
		}
		File tempDirFile = new File(tempDir);

		if (!tempDirFile.exists())
			tempDirFile.mkdirs();

		System.setProperty("java.io.tmpdir", tempDir);

		logger.debug("initializing temp directory");

		for (Props setting : props) {
			setting.loadSettings(null, prop, null);
		}
		return prop;
	}

	public Settings loadConfigurationFile(MailArchivaPrincipal principal)
			throws ConfigurationException {
		String fileName = getServerConfFile();
		logger.debug("loading server settings {location='" + fileName + "'}");
		Settings prop = Settings.loadProperties(fileName, "UTF-8");

		// record this event
		Settings debugoutput = (Settings) prop.clone();
		debugoutput.setProperty("security.passhrase", "<hidden>");
		audit.info("load config " + debugoutput.toString() + ", " + principal
				+ "}");
		logger.debug(debugoutput.toString());
		return prop;
	}

	private String newTempDir() {
		String tmpDir = System.getProperty("java.io.tmpdir");
		if (tmpDir.charAt(tmpDir.length() - 1) == File.separatorChar)
			tmpDir = tmpDir.substring(0, tmpDir.length() - 1);
		if (tmpDir == null || tmpDir.length() < 2) {
			tmpDir = File.separatorChar + "tmp" + File.separatorChar
					+ FileSystem.getProductName().toLowerCase(Locale.ENGLISH);
		} else {
			if (!tmpDir.contains(FileSystem.getProductName().toLowerCase(
					Locale.ENGLISH))) {
				tmpDir = tmpDir
						+ File.separatorChar
						+ FileSystem.getProductName().toLowerCase(
								Locale.ENGLISH);
			}
		}
		return tmpDir;
	}

	private void setSalt(String saltStr) {
        if (saltStr==null) {
      	  // we fake the salt for the moment, as there is too much of a risk that
      	  // the admin will lose their entire store because they forgot to 
      	  // copy the server.conf file (which contains their salt value)
  	  	  //salt = new byte[8];
            //new Random().nextBytes(salt);
      	  salt = ConfigUtil.fromHex("feadf944dd4d62a5");
  	  	} else {
  	  	  salt = ConfigUtil.fromHex(saltStr);
  	  	}
}
	
	public void setPBEAlgorithm(String pbeAlgorithm) {
		this.pbeAlgorithm = pbeAlgorithm;
	}

	public String getServerConfFile() {
		return filesystem.getConfigurationPath() + File.separatorChar
				+ "server.conf";
	}

	public Indexer getIndex() {
		return indexer;
	}

	public Volumes getVolumes() {
		return volumes;
	}

	public void setVolumes(Volumes volumes) {
		this.volumes = volumes;
	}

	public Archiver getArchiver() {
		return archiver;
	}

	public VolumeIRService getVolumeIRService() {
		return volumeIRService;
	}

	public byte[] getSalt() {
		return salt;
	}

	public static FileSystem getFileSystem() {
		return filesystem;
	}

	public String getPBEAlgorithm() {
		return pbeAlgorithm;
	}

	public Search getSearch() {
		return search;
	}

	public ConfigAutoLoadService getConfigAutoLoadService() {
		return configAutoLoad;
	}

	public EmailFields getEmailFields() {
		return emailFields;
	}

	public synchronized void saveSettings(MailArchivaPrincipal principal, boolean updateRemoteServers) throws ConfigurationException {
	  	// we dont want changes to trigger configautoload service
	  try {
		configAutoLoad.block();
		volumeIRService.block();
	  	volumes.saveAllVolumeInfo(true);
	    saveConfiguration(principal);
//		basicIdentity.saveXMLFile();
//		adIdentity.saveHostsFileEntry();
	  } finally {
		  configAutoLoad.unblock();
		  volumeIRService.unblock();
	  }
	  
  }
	public synchronized Settings saveConfiguration(MailArchivaPrincipal principal) throws ConfigurationException {
	    Settings settings = getSettings();
		saveConfigurationFile(principal,settings);
		return settings;
    }
	
	 public void saveConfigurationFile(MailArchivaPrincipal principal, Settings settings) throws ConfigurationException {
			
			synchronized(readWriteLock) {
				String fileName = getServerConfFile();
				logger.debug("saving Settings {location='"+fileName+"'}");
			
				logger.debug(settings.toString());
				audit.info("update config "+ settings.toString()+", "+principal+"}");
				
				String intro =  "# "+Config.getConfig().getProductName().toUpperCase(Locale.ENGLISH)+" Settings File" + System.getProperty("line.separator")+
								"# Copyright Jamie Band 2008" + System.getProperty("line.separator");
			
				Settings.saveProperties(fileName, intro, settings,"UTF-8");
			}
		
		}
	 
	 
	 public Settings getSettings() { 
		 	Settings currentConfiguration = new Settings();
		 	currentConfiguration.setProperty(pbeAlgorithmKey,getPBEAlgorithm());
			if (salt!=null)
				currentConfiguration.setProperty(saltKey,ConfigUtil.toHex(getSalt()));
			currentConfiguration.setProperty(tempDirKey, tempDir);
			for (Props settings : props) {
				settings.saveSettings(null,currentConfiguration,null);
			}

			return currentConfiguration;

	 }
	
	public static String getEncKey() {
		return "tQwe4rZdfjerosd23912As23z";
	}

	protected ArchiveFilter archiveFilter;

	public ArchiveFilter getArchiveFilter() {
		return archiveFilter;
	}

	protected Domains domains;

	public Domains getDomains() {
		return domains;
	}

	public static boolean getShutdown() {
		return shutdown;
	}

	public String getProductName() {
		return FileSystem.getProductName();
	}

	protected Roles roles;

	public Roles getRoles() {
		return roles;
	}

	public interface UpdateObserver {

		public void updateConfig();
	}

	protected static WeakHashMap<UpdateObserver, UpdateObserver> updateObservers;

	public void registerUpdateObserver(UpdateObserver observer) {
		updateObservers.put(observer, observer);
	}

	public void unregisterUpdateObserver(UpdateObserver observer) {
		updateObservers.remove(observer);
	}

	public void notifyUpdateObservers() {
		for (UpdateObserver observer : updateObservers.values()) {
			observer.updateConfig();
		}
	}

	protected Services services;

	public Services getServices() {
		return services;
	}

	public void registerServices() {
		// services.registerService(smtpService);
		// services.registerService(milterService);
		// services.registerService(iapService);
		services.registerService(indexer);
		services.registerService(volumeIRService);
		services.registerService(configAutoLoad);
		// services.registerService(volumeInfoService);
		// services.registerService(new ReArchiveService());
		// services.registerService(throttleService);
	}
	 protected void registerProps() {
		 props = new ArrayList<Props>();
//		 props.add(agent);
//		 props.add(milterService);
//		 props.add(smtpService);
		 props.add(archiveFilter);
//		 props.add(authentication);
		 props.add(domains);
//		 props.add(adIdentity);
//		 props.add(basicIdentity);
//		 props.add(mailboxConnections);
		
		 props.add(archiver);
		 props.add(volumes);
		 props.add(search);
		 props.add(indexer);
		 props.add(roles);
//		 props.add(throttleService);
	 }
}
