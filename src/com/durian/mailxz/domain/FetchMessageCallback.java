package com.durian.mailxz.domain;

import java.io.InputStream;

import com.durian.mailxz.exception.ArchiveException;


public interface FetchMessageCallback {

	public void store(InputStream is, String remoteIP) throws ArchiveException;
}
