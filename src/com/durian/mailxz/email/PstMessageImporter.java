package com.durian.mailxz.email;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import com.pff.PSTAttachment;
import com.pff.PSTException;
import com.pff.PSTFile;
import com.pff.PSTFolder;
import com.pff.PSTMessage;

public class PstMessageImporter implements IMessageImporter {
	private String pstFilePath;
	private String importFolder;
//	private boolean  
	
	@Override
	public void importMessage() {
		try {
			PSTFile pstFile = new PSTFile(pstFilePath);
			System.out.println(pstFile.getMessageStore().getDisplayName());
			
			PSTFolder folder = pstFile.getRootFolder();
			if (folder.hasSubfolders()) {
                Vector<PSTFolder> childFolders = folder.getSubFolders();
                for (PSTFolder childFolder : childFolders) {
                        processFolder(childFolder);
                }
        }
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PSTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		long id = email.getDescriptorNodeId();
//		pstObject = PSTObject.detectAndLoadPSTObject(pstFile, id);
	}

	private void processFolder(PSTFolder folder) throws PSTException, IOException {
		// TODO Auto-generated method stub
		if (folder.getContentCount() > 0) {
            PSTMessage email = (PSTMessage)folder.getNextChild();
            while (email != null) {
                    System.out.println("Email: "+email.getSubject());
                    email = (PSTMessage)folder.getNextChild();
            }
    }
	}
	
	public void saveAttachment(PSTMessage email) throws Exception, PSTException {
		int numberOfAttachments = email.getNumberOfAttachments();
		for (int x = 0; x < numberOfAttachments; x++) {
		        PSTAttachment attach = email.getAttachment(x);
		        InputStream attachmentStream = attach.getFileInputStream();
		        // both long and short filenames can be used for attachments
		        String filename = attach.getLongFilename();
		        if (filename.isEmpty()) {
		                filename = attach.getFilename();
		        }
		        FileOutputStream out = new FileOutputStream(filename);
		        // 8176 is the block size used internally and should give the best performance
		        int bufferSize = 8176;
		        byte[] buffer = new byte[bufferSize];
		        int count = attachmentStream.read(buffer);
		        while (count == bufferSize) {
		                out.write(buffer);
		                count = attachmentStream.read(buffer);
		        }
		        byte[] endBuffer = new byte[count];
		        System.arraycopy(buffer, 0, endBuffer, 0, count);
		        out.write(endBuffer);
		        out.close();
		        attachmentStream.close();
		}
	}

	public String getPstFilePath() {
		return pstFilePath;
	}

	public void setPstFilePath(String pstFilePath) {
		this.pstFilePath = pstFilePath;
		 
		 
	}

}
