package com.durian.mailxz;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.durian.mailxz.domain.Config;
import com.durian.mailxz.domain.FileSystem;
import com.durian.mailxz.domain.MailArchivaPrincipal;
import com.durian.mailxz.service.MessageService;

public class Startup {
	protected static final Log logger = LogFactory.getLog(Startup.class);

	public void init() {
		try {
			Config conf = Config.getConfig();
			
			String appPath = "D:\\Development\\mailxz";

			FileSystem fs = Config.getFileSystem();
			fs.outputSystemInfo();
			
			fs.setApplicationPath(appPath);
			if (!fs.checkAllSystemPaths()) {
				logger.fatal("server cannot find one or more required system paths.");
				System.exit(1);
			}
			if (!fs.checkStartupPermissions()) {
				logger.fatal("failed to startup. directory and file read/write permissions not defined correctly.");
				System.exit(1);
			}
			fs.initLogging();
			fs.initCrypto();
			fs.clearViewDirectory();
			fs.clearTempDirectory();

			conf.init(MessageService.getFetchMessageCallback());
			conf.loadSettings(MailArchivaPrincipal.SYSTEM_PRINCIPAL);
			conf.registerServices();
			conf.getServices().startAll();
			logger.debug("startup sequence is complete");
		} catch (Exception e) {
			logger.fatal("failed to execute startup cause: ", e);
			throw new RuntimeException(e);
//			System.exit(1);
//			return;
		}
	}

	public void destroy() {
		Config.shutdown();
	}
}
