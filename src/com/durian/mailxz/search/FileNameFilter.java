package com.durian.mailxz.search;


/* Copyright (C) 2005-2007 Jamie Angus Band 
 * MailArchiva Open Source Edition Copyright (c) 2005-2007 Jamie Angus Band
 * This program is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see http://www.gnu.org/licenses or write to the Free Software Foundation,Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

import java.io.IOException;
import java.io.Serializable;
import java.util.Stack;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Token;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;


public class FileNameFilter extends TokenFilter  implements Serializable {

	private static final long serialVersionUID = -5280125822406627625L;

	protected static final Log logger = LogFactory.getLog(FileNameFilter.class.getName());
	
	private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
	private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
	private final PositionIncrementAttribute posAtt = addAttribute(PositionIncrementAttribute.class);
	
    private Stack<Token> filenameTokenStack;
   
    public FileNameFilter(TokenStream in) {
        super(in);
        filenameTokenStack = new Stack<Token>();
    }

    @Override
    public boolean incrementToken() throws IOException {
    	if (filenameTokenStack.size() > 0) {
    		putFromToken(filenameTokenStack.pop());
    		return true;
    	}
    	
    	if (input.incrementToken()) {
    		putPart(getInToken());
    		return true;
    	}
    	return false;
    }
        
    private void putFromToken(Token tk) {
		termAtt.copyBuffer(tk.buffer(), 0, tk.length());
		termAtt.setLength(tk.length());
		offsetAtt.setOffset(tk.startOffset(), tk.endOffset());
		posAtt.setPositionIncrement(tk.getPositionIncrement());
    }
    
    private Token getInToken() {
    	Token token = new Token();
    	token.copyBuffer(termAtt.buffer(), 0, termAtt.length());
    	token.setLength(termAtt.length());
    	token.setOffset(offsetAtt.startOffset(), offsetAtt.endOffset());
    	token.setPositionIncrement(posAtt.getPositionIncrement());
    	return token;
    }
/*
    @Override
	public Token next() throws IOException {

        if (filenameTokenStack.size() > 0) 
            return filenameTokenStack.pop();

        Token token = input.next();
       
        if (token == null) 
            return null;

        putPart(token);

        return token;
    }*/
     
    private void putPart(Token token) throws IOException {
    	String fileName = new String(token.buffer(), 0, token.length());
    	if (fileName.indexOf('.')!=-1) {
    		String[] splitOnDot = fileName.split("\\.");
    		Token nameToken = new Token(splitOnDot[0].trim(),token.startOffset(),token.endOffset());
    		Token extToken = new Token(splitOnDot[1].trim(),token.startOffset(),token.endOffset());
    		filenameTokenStack.push(nameToken);
    		filenameTokenStack.push(extToken);
    	}
    }


}
