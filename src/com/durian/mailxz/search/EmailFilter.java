/* Copyright (C) 2005-2007 Jamie Angus Band 
 * MailArchiva Open Source Edition Copyright (c) 2005-2007 Jamie Angus Band
 * This program is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see http://www.gnu.org/licenses or write to the Free Software Foundation,Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

package com.durian.mailxz.search;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Stack;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;

/* Many thanks to Michael J. Prichard" <michael_prich...@mac.com> for his
 * original the email filter code. It is rewritten. */

public class EmailFilter extends TokenFilter implements Serializable {
	private static final long serialVersionUID = -5280145822406627625L;
	
	 
	protected static final Log logger = LogFactory.getLog(EmailFilter.class
			.getName());

	private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
	private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
	private final PositionIncrementAttribute posAtt = addAttribute(PositionIncrementAttribute.class);
	
	private int startOffset;
	private int enOffset;
	
	
	private Stack<String> emailPartStack;
	
	public EmailFilter(TokenStream in) {
		super(in);
		emailPartStack = new Stack<String>();
	}

	@Override
	public boolean incrementToken() throws IOException {
		final char[] buffer = termAtt.buffer();
		
		if (emailPartStack.size() > 0) {
			String pt = emailPartStack.pop();
			pt.getChars(0, pt.length(), buffer, 0);
			termAtt.setLength(pt.length());
			offsetAtt.setOffset(startOffset, enOffset);
			posAtt.setPositionIncrement(0);
			return true;
		}
		
		if (!input.incrementToken()) {
			return false;
		}
		putPart();
		return true;
	}

	private void putPart() throws IOException {
		String emailAddress = new String(termAtt.buffer(), 0, termAtt.length());
		emailAddress = emailAddress.replaceAll("<", "");
		emailAddress = emailAddress.replaceAll(">", "");
		emailAddress = emailAddress.replaceAll("\"", "");

		String[] parts = extractEmailParts(emailAddress);
		
		for (int i = 0; i < parts.length; i++) {
			String s = parts[i];
			if (null != s) {
				emailPartStack.push(s);
			}
		}
		startOffset = offsetAtt.startOffset();
		enOffset = offsetAtt.endOffset();
	}

	private String[] extractWhitespaceParts(String email) {
		String[] whitespaceParts = email.split(" ");
		ArrayList<String> partsList = new ArrayList<String>();
		for (int i = 0; i < whitespaceParts.length; i++) {
			partsList.add(whitespaceParts[i]);
		}
		return whitespaceParts;
	}

	private String[] extractEmailParts(String email) {

		if (email.indexOf('@') == -1)
			return extractWhitespaceParts(email);

		ArrayList<String> partsList = new ArrayList<String>();

		String[] whitespaceParts = extractWhitespaceParts(email);

		for (int w = 0; w < whitespaceParts.length; w++) {

			if (whitespaceParts[w].indexOf('@') == -1)
				partsList.add(whitespaceParts[w]);
			else {
				partsList.add(whitespaceParts[w]);
				String[] splitOnAmpersand = whitespaceParts[w].split("@");
				try {
					partsList.add(splitOnAmpersand[0]);
					partsList.add(splitOnAmpersand[1]);
				} catch (ArrayIndexOutOfBoundsException ae) {
				}

				if (splitOnAmpersand.length > 0) {
					String[] splitOnDot = splitOnAmpersand[0].split("\\.");
					for (int i = 0; i < splitOnDot.length; i++) {
						partsList.add(splitOnDot[i]);
					}
				}
				if (splitOnAmpersand.length > 1) {
					String[] splitOnDot = splitOnAmpersand[1].split("\\.");
					for (int i = 0; i < splitOnDot.length; i++) {
						partsList.add(splitOnDot[i]);
					}

					if (splitOnDot.length > 2) {
						String domain = splitOnDot[splitOnDot.length - 2] + "."
								+ splitOnDot[splitOnDot.length - 1];
						partsList.add(domain);
					}
				}
			}
		}
		return partsList.toArray(new String[0]);
	}
}
