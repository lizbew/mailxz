package com.durian.mailxz.ui.wizard;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.wizard.Wizard;

import com.durian.mailxz.domain.Config;

public class ImportPSTWizard extends Wizard {
	private ImportPSTPrefs prefs;
	
	public ImportPSTWizard() {
		super();
		prefs =  new ImportPSTPrefs();
		prefs.setImportPath(Config.getDefault().lastImportPath);
	}
	
	@Override
	public void addPages() {
		addPage(new SummaryWizardPage(prefs));
		addPage(new DirectoryWizardPage(prefs));
	}

	@Override
	public boolean performFinish() {
		DirectoryWizardPage directoryPage = (DirectoryWizardPage)getPage(DirectoryWizardPage.PAGE_NAME);
		String filePath = prefs.getImportPath();
		if (filePath == null) {
			return true;
		}
		
		if (filePath.endsWith(".msg")) {
			
		}
		return true;
	}
	
	public class ImportPSTJob extends Job {

		public ImportPSTJob(String name) {
			super(name);
			setUser(true);
			setPriority(Job.SHORT);
			schedule();
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			try{
			final IProgressMonitor m = monitor;
//		     ProcessorTaskProvider provider = ProcessorTaskProvider
//		       .getInstance();
//		     provider.executeTask(m);
			monitor.beginTask("sr...", 10);  
			 monitor.worked(10);
			 monitor.done(); 
		    } catch (Exception e) {
		     e.printStackTrace();
		    }
			return Status.OK_STATUS;
		}
		/*job.schedule(1133);//delaytime 
        job.setUser(true);//if true show UI 
        job.setPriority(priority)*/ 
	}
}
