package com.durian.mailxz.ui.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class DirectoryWizardPage extends WizardPage {
	public static final String PAGE_NAME = "DirectoryPage";
	public static final String PAGE_DESCRIPTION = "Choose a file(or from a directory) to import";
	
	private Text text;
	private Button button;
	private ImportPSTPrefs prefs;
	
	public DirectoryWizardPage(ImportPSTPrefs prefs) {
		super(PAGE_NAME);
		setDescription(PAGE_DESCRIPTION);
		this.prefs = prefs;
	}
	
	@Override
	public void createControl(Composite parent) {
		Composite top = new Composite(parent, SWT.NONE);
		top.setLayout(new GridLayout(3, false));
		
		Label label = new Label(top, SWT.CENTER);
		label.setText("From File:");
		
		text = new Text(top, SWT.SINGLE|SWT.BORDER);
		text.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		
		button = new Button(top, SWT.PUSH);
		button.setText("&Browse...");
		
		final DirectoryWizardPage selfPage = this;
		
		button.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				String path = prefs.getImportPath();
				if (ImportPSTPrefs.ImportTypeEnum.PST_FILE.equals(prefs.getImportType()) || ImportPSTPrefs.ImportTypeEnum.MSG_FILE.equals(prefs.getImportType())) {
					FileDialog dialog = new FileDialog (e.display.getActiveShell(), SWT.OPEN);
					String [] filterNames = new String [] {"PST File", "All Files (*)"};
					String [] filterExtensions = new String [] {"*.pst;*.msg", "*.*"};
					
					dialog.setFilterNames (filterNames);
					dialog.setFilterExtensions (filterExtensions);
					dialog.setFilterPath (path);
					path = dialog.open ();
					
				} else if (ImportPSTPrefs.ImportTypeEnum.MSG_DIR.equals(prefs.getImportType())) {
					DirectoryDialog dialog = new DirectoryDialog(e.display.getActiveShell());
					dialog.setFilterPath(path);
					path = dialog.open();
				}
			
				if (null != path && !path.isEmpty()) {
					text.setText(path);
					selfPage.setPageComplete(true);
				}
				
//				MessageDialog.openInformation(e.display.getActiveShell(), "Open", path);
			}
		});
		
		setControl(top);
		this.setPageComplete(false);
	}
}
