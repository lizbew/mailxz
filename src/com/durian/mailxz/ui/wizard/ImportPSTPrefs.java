package com.durian.mailxz.ui.wizard;

public class ImportPSTPrefs {
	public static String[] importTypes = new String[] {"PST file", "msg file", "msg file in folder"};
	public static final String DEFAULT_IMPORT_PATH = ".";
	public static final ImportTypeEnum DEFAULT_IMPORT_TYPE = ImportTypeEnum.PST_FILE;
	
	private String importPath = DEFAULT_IMPORT_PATH;
	private ImportTypeEnum importType = DEFAULT_IMPORT_TYPE;
	
	public String getImportPath() {
		return importPath;
	}
	
	public void setImportPath(String importPath) {
		this.importPath = importPath;
	}
	
	public ImportTypeEnum getImportType() {
		return importType;
	}
	
	public void setImportType(String typeDesc) {
		setImportType(ImportTypeEnum.getImportTypeByDescription(typeDesc));
	} 
	
	public void setImportType(ImportTypeEnum importType) {
		this.importType = importType;
	}
	
	public static String[] getAllImportTypes() {
		return ImportTypeEnum.getImportTypeDescArray();
	}
	
	public enum ImportTypeEnum {
		PST_FILE("PST_FILE", "PST file"),
		MSG_FILE("MSG_FILE", "msg file"),
		MSG_DIR("MSG_DIR", "msg file folder");
		
		private String value;
		private String description;
		
		private ImportTypeEnum(String v, String d) {
			value = v;
			description = d;
		}

		public String getValue() {
			return value;
		}

		public String getDescription() {
			return description;
		}
		
		public static String[] getImportTypeDescArray() {
			int len = ImportTypeEnum.values().length;
			String[] descArray = new String[len];
			int i = 0;
			for (ImportTypeEnum e : ImportTypeEnum.values()) {
				descArray[i] = e.getDescription();
				i++;
			}
			
			return descArray;
		}
		
		public static ImportTypeEnum getImportTypeByDescription(String desc) {
			for (ImportTypeEnum e : ImportTypeEnum.values()) {
				if (e.description.equals(desc)) {
					return e;
				}
			}
			return null;
		}
	}
}
