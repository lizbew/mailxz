package com.durian.mailxz.ui.wizard;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class SummaryWizardPage extends WizardPage {
	public static final String PAGE_NAME = "SummaryPage";
	private ImportPSTPrefs prefs;
	
	public SummaryWizardPage(ImportPSTPrefs prefs) {
		super(PAGE_NAME);
		setDescription("Summary");
		this.prefs = prefs;
	}
	
	@Override
	public void createControl(Composite parent) {
		Composite top = new Composite(parent, SWT.NONE);
		top.setLayout(new GridLayout(2, false));
		
		Label label = new Label(top, SWT.CENTER);
		label.setText("Import:");
		
		Combo combo = new Combo(top, SWT.READ_ONLY);
		combo.setItems(ImportPSTPrefs.getAllImportTypes());
		combo.select(0);
		combo.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				prefs.setImportType(((Combo)e.widget).getText());
			}
		});
		
		setControl(top);
	}
}
