package com.durian.mailxz.ui.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_PATH = "pathPreference";

	public static final String P_BOOLEAN = "booleanPreference";

	public static final String P_CHOICE = "choicePreference";

	public static final String P_STRING = "stringPreference";
	
	public static final String P_MAIL_SERVER_URL = "mailServerURLPreference";
	public static final String P_MAILBOX_USER = "vika";
	public static final String P_MAILBOX_PASSWORD = "pwd";
	
	public static final String P_REPOSITRY_ROOT = "repositryRootPreference";
}
