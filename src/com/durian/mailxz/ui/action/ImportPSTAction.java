package com.durian.mailxz.ui.action;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;

import com.durian.mailxz.ui.ICommandIds;
import com.durian.mailxz.ui.wizard.ImportPSTWizard;

import org.eclipse.jface.wizard.WizardDialog;

public class ImportPSTAction extends Action {
	private final IWorkbenchWindow window;
	
	public ImportPSTAction(String text, IWorkbenchWindow window) {
		super(text);
        this.window = window;
     // The id is used to refer to the action in a menu or toolbar
        setId(ICommandIds.CMD_OPEN_MESSAGE);
        // Associate the action with a pre-defined command, to allow key bindings.
        setActionDefinitionId(ICommandIds.CMD_OPEN_MESSAGE);
        setImageDescriptor(com.durian.mailxz.Activator.getImageDescriptor("/icons/sample3.gif"));
        
	}
	
	@Override
	public void run() {
		WizardDialog wizDialog = new WizardDialog(window.getShell(), new ImportPSTWizard());
		wizDialog.open();
	}
}
